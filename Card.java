public class Card{
	//establish suit and value fields
	private String suit;
	private String value;
	
	//constructor that takes in two String inputs 
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}
	
	//getters
	public String getSuit(){
		return this.suit;
	}
	
	public String getValue(){
		return this.value;
	}
	
	//toString method
	public String toString(){
		if(this.value.equals("1")){
			return "Ace" + " of " + this.suit;
		}
		if(this.value.equals("11")){
			return "Jack" + " of " + this.suit;
		}
		if(this.value.equals("12")){
			return "Queen" + " of " + this.suit;
		}
		if(this.value.equals("13")){
			return "King" + " of " + this.suit;
		}
		return this.value + " of " + this.suit;
	}
	
	//method that returns the score of the card based on it's value and suit. The score is the value of the card + 0.4 if the suit is Hearts, 0.3 if the suit is Diamonds, 0.2 if the suit is Clubs, and 0.1 if the suit is Spades. The score is returned as a double.
	public double calculateScore(){
		double score = 0.0;
		double cardValue = Integer.parseInt(this.value);
		if(this.suit.equals("Hearts")){
			score = cardValue + 0.4;
		}
		else if(this.suit.equals("Diamonds")){
			score = cardValue + 0.3;
		}
		
		else if(this.suit.equals("Clubs")){
			score = cardValue + 0.2;
		}
		else{
			score = cardValue + 0.1;
		}
		return score;
	}
}