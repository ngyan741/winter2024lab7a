import java.util.Random;
public class Deck{
	//establish fields, including an array of Cards
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	//constructor that creates the deck and fills every single space in the cards array. It starts with hearts and loops over every single value, then Diamonds, then spaces, then clubs. It also establishes the numberOfCards variable and creates the random object.
	public Deck(){
		this.numberOfCards = 52;
		this.rng = new Random();
		this.cards = new Card [52];
		String[] suits = new String[]{"Hearts", "Diamonds", "Spades", "Clubs"};
		String[] values = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"};
		int counter = 0;
		for(int i=0; i<suits.length; i++){
			for(int j=0; j<values.length; j++){				
				this.cards[counter] = new Card(suits[i], values[j]);
				counter++;
			}
		}
		
	}
	
	//method that returns the numberOfCards, acts as a getter 
	public int length(){
		return numberOfCards;
	}
	
	//method that "removes" one card from the deck, and returns the card in the last position 
	public Card drawTopCard(){
		numberOfCards--;
		return this.cards[numberOfCards];
	}

	//toString method that uses an empty builder string. It loops over the cards array and adds it to the builder string, then returns it.
	public String toString(){
		String builder = "";
		for(int i=0; i<numberOfCards; i++){
			builder = builder + i + " " + this.cards[i] + "\n";
		}
		return builder;
	}
	
	//method that uses loops over the cards array, takes a random index from the array and swaps the card with the current index of the array 
	public void shuffle(){
		int randomNumber = 0;
		Card tempStorage = new Card("", "");
		for(int i=0; i<numberOfCards; i++){
			randomNumber = this.rng.nextInt(i, numberOfCards);
			tempStorage = this.cards[i];
			this.cards[i] = this.cards[randomNumber];
			this.cards[randomNumber] = tempStorage;
		}
	}

}